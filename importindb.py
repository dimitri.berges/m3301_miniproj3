import sqlite3
import logging

def import2DB(nomCSV, contentCSV):
    logging.debug("Début la gestion de BDD")
    aimporter = contentCSV[1:]
    logging.debug("Création de la BDD")
    
    connection = sqlite3.connect(nomCSV[:len(nomCSV)-3] + "sqlite3")
    cursor = connection.cursor()
    logging.debug("Base SQLite3 créées ou connectées")

    logging.info("Création de la table SQL si elle n'existe pas")
    cursor.execute("CREATE TABLE IF NOT EXISTS 'SIV' (adresse_titulaire text, nom text, prenom text, immatriculation text primary key, date_immatriculation date, vin numeric, marque text, denomination_commerciale text, couleur text, carrosserie text, categorie text, cylindree numeric, energie numeric, places integer, poids integer, puissance integer, type text, variante text, version text);")
    count = [0, 0]
    for row in aimporter:
        result = cursor.execute("SELECT COUNT(*) FROM 'SIV' WHERE immatriculation = ?", (row[3], )).fetchone()
        if result[0] == 1:
            row.remove(row[3])
            row.append(row[3])
            cursor.execute("UPDATE 'SIV' SET adresse_titulaire = ?, nom = ?, prenom = ?, date_immatriculation = ?, vin = ?, marque = ?, denomination_commerciale = ?, couleur = ?, carrosserie = ?, categorie = ?, cylindree = ?, energie = ?, places = ?, poids = ?, puissance = ?, type = ?, variante = ?, version = ? WHERE immatriculation = ?", tuple(row))
            count[0] += 1
        else:
            cursor.execute("INSERT INTO 'SIV' VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", tuple(row))
            count[1] += 1
    connection.commit()
    logging.info("Données importées")
    logging.info(f"{count[0]} lignes modifiées")
    logging.info(f"{count[1]} lignes créées")