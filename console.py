import argparse
import os
from readcsv import lectureCSV
import logging
import datetime
from importindb import import2DB

logging.basicConfig(filename=f"logs/importBDD {datetime.datetime.now()}.log", level=logging.DEBUG, format="%(asctime)s %(levelname)s %(message)s")

parser = argparse.ArgumentParser(description="Script de modification du format d'un .csv")
parser.add_argument("fichier", help="le chemin relatif ou absolu du fichier à traiter")
parser.add_argument("-d", "--delimiter", help="Délimiteur du CSV si ce n'est pas \";\"", default=";")
args = parser.parse_args()

if (not os.path.exists(args.fichier)):
    logging.critical("Le fichier " + args.fichier + " n'existe pas :(")
    exit(1)

if (not os.path.isfile(args.fichier)):
    logging.critical(args.fichier + " n'est pas un fichier :(")
    exit(2)

fileContent = lectureCSV(args.fichier, args.delimiter)

if fileContent == None:
    logging.error("Erreur de lecture :(")
    exit(3)

import2DB(args.fichier, fileContent)
logging.debug("Fin de l'éxecution du programme")
exit(0)